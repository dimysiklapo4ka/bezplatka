package dimysiklapo4ka.com.bezplatka

object AdvertisementUtils{

    fun formatPrice(price: String): String {
        if (price.isNotEmpty()){
            try {
                price.toLong()
            }catch (e:Exception){
                val splitedPrice = price.split(" ")
                return "${splitedPrice[0]} UAH"
            }
            return "$price UAH"
        }
        return price
    }

}