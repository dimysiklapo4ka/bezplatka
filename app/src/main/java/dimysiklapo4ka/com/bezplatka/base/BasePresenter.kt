package dimysiklapo4ka.com.bezplatka.base

interface BasePresenter<V: BaseView>{
    fun attach(view: V)
    fun dettach()
}