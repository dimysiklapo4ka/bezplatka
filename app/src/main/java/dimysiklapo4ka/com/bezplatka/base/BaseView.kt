package dimysiklapo4ka.com.bezplatka.base

import android.support.v7.app.AlertDialog

interface BaseView{
    fun exitDialog(): AlertDialog
}