package dimysiklapo4ka.com.bezplatka.base

open class BasePresenterImplementation<V: BaseView >: BasePresenter<V>{

    protected var view: V? = null

    override fun attach(view: V) {
        this.view = view
    }

    override fun dettach() {
        view = null
    }

}