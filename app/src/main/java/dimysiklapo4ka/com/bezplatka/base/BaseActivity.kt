package dimysiklapo4ka.com.bezplatka.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel
import dimysiklapo4ka.com.bezplatka.replaceFragment

abstract class BaseActivity<V: BaseView, P: BasePresenter<V>>: AppCompatActivity(), BaseView{

    protected abstract val resLayout: Int
    protected abstract var presenter: P?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(resLayout)
        presenter?.attach(this as V)
        init()
    }

    override fun onDestroy() {
        presenter?.dettach()
        super.onDestroy()
        presenter = null
    }

    override fun exitDialog(): AlertDialog = AlertDialog.Builder(this).
        setTitle("Выйти из приложения?").
        setMessage("Вы уверены что хотите Выйти из приложения").
        setPositiveButton("Выйти") { _, _ ->
            finish()
        }.
        setNegativeButton("Остаться") { dialog, _ ->
            dialog.dismiss()
        }.
        create()

    abstract fun advertisementClicked(advertisement: DataSnapshot)

    protected abstract fun init()

    fun replacesFragment(container: Int, fragment: Fragment){
        replaceFragment(container, fragment, true)
    }
}