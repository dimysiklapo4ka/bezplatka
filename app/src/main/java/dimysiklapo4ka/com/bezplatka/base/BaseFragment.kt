package dimysiklapo4ka.com.bezplatka.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment<V: BaseView, P: BasePresenter<V>>: Fragment(){

    protected abstract val resLayout: Int
    protected abstract var presenter: P?

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(resLayout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter?.attach(activity as V)
        init()
    }

    override fun onDestroyView() {
        presenter?.dettach()
        super.onDestroyView()
        presenter = null
    }

    protected abstract fun init()

}