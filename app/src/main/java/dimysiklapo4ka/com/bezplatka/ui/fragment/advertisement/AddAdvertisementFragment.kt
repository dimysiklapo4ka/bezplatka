package dimysiklapo4ka.com.bezplatka.ui.fragment.advertisement

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.AdvertisementUtils
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainActivity
import dimysiklapo4ka.com.bezplatka.R
import dimysiklapo4ka.com.bezplatka.base.BaseFragment
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainContract
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainPresenter
import dimysiklapo4ka.com.bezplatka.ui.fragment.main.MainFragment
import dimysiklapo4ka.com.bezplatka.ui.listeners.OnAdvertisementItemUpdateListeners
import kotlinx.android.synthetic.main.fragment_add_advertisement.*

class AddAdvertisementFragment : BaseFragment<MainContract.MainView, MainContract.MainPresenter>(),
    OnAdvertisementItemUpdateListeners {

    override val resLayout: Int = R.layout.fragment_add_advertisement
    override var presenter: MainContract.MainPresenter? = MainPresenter()

    private var dataSnapshot: DataSnapshot? = null
    private var adv: AdvertisementModel? = null

    override fun init() {

        if (adv != null) {
            setDataFromUpdate(adv)
        }
        addedAdvertisement.setOnClickListener {
            visibilityAddButton(advertisementTitle.text.toString(), advertisementDescription.text.toString(),
                AdvertisementUtils.formatPrice(advertisementPrice.text.toString()), advertisementCity.text.toString(),
                advertisementNameSaler.text.toString(), advertisementPhoneNumber.text.toString())
        }

        formatedTexts()
    }

    override fun onAdvertisementItemUpdate(advertisement: DataSnapshot) {
        dataSnapshot = advertisement
        adv = advertisement.getValue(AdvertisementModel::class.java)
    }

    private fun setDataFromUpdate(advertisement: AdvertisementModel?) {
        advertisementTitle.setText(advertisement?.title.toString())
        advertisementDescription.setText(advertisement?.description.toString())
        advertisementPrice.setText(advertisement?.price.toString())
        advertisementCity.setText(advertisement?.city.toString())
        advertisementNameSaler.setText(advertisement?.name_saler.toString())
        advertisementPhoneNumber.setText(advertisement?.phone_number.toString())
    }

    private fun visibilityAddButton(title: String, description: String, price: String, country: String,
                                    saler_name: String, phone_number: String){

        if (title.isEmpty()) advertisementTitle.error = "Не заполнен Заголовок"
        if (description.isEmpty() || description.length > 500) advertisementDescription.error = "Нет описания товара"
        if (price.isEmpty()) advertisementPrice.error = "Добавьте цену"
        if (country.isEmpty()) advertisementCity.error = "Заполните город"
        if (saler_name.isEmpty()) advertisementNameSaler.error = "Укажите Ваше Им`я"
        if (phone_number.isEmpty() || phone_number.length < 13) advertisementPhoneNumber.error = "Укажите ваш номер телефона"

        if (title.isNotEmpty() && description.isNotEmpty() && description.length <= 500 && price.isNotEmpty() && country.isNotEmpty()
            && saler_name.isNotEmpty() && phone_number.isNotEmpty() && phone_number.length >= 13){


            presenter?.addedOrUpdateItemInFirebaseDatabase(
                dataSnapshot, title,description,price,country,saler_name, phone_number)

            (activity as MainActivity).replacesFragment(R.id.container, MainFragment())
            dataSnapshot = null
            adv = null

        }else{
            Toast.makeText(activity, "Заполните поля", Toast.LENGTH_SHORT).show()
        }
    }


    private fun formatedTexts() {

        advertisementDescription.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (count >= 500){
                    advertisementDescription.error = "Превышен лимит вводимых символов"
                }
            }
        })

        advertisementPhoneNumber.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (start > 3){
                    if (!s.toString().contains("+380")){
                        advertisementPhoneNumber.error = "Не верный формат номера.\n+38(код оператора)номер телефона"
                    }
                }
            }
        })
    }
}