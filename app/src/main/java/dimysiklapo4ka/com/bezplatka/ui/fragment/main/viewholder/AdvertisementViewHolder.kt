package dimysiklapo4ka.com.bezplatka.ui.fragment.main.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainActivity
import kotlinx.android.synthetic.main.view_item_advertisement.view.*

class AdvertisementViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bindView(advertisement: DataSnapshot){
        val adv: AdvertisementModel = advertisement.getValue(AdvertisementModel::class.java)!!
        itemView.title.text = adv.title
        itemView.description.text = adv.description
        itemView.price.text = adv.price
        itemView.city.text = adv.city
        itemView.name_saler.text = adv.name_saler
        itemView.phone_number.text = adv.phone_number

        itemView.setOnClickListener{(itemView.context as MainActivity).advertisementClicked(advertisement)}
    }

}