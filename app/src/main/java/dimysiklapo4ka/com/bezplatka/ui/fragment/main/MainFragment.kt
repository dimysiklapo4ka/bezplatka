package dimysiklapo4ka.com.bezplatka.ui.fragment.main

import android.support.v7.widget.LinearLayoutManager
import com.google.firebase.database.*
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainActivity
import dimysiklapo4ka.com.bezplatka.R
import kotlinx.android.synthetic.main.fragment_main.*
import android.util.Log
import dimysiklapo4ka.com.bezplatka.base.BaseFragment
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainContract
import dimysiklapo4ka.com.bezplatka.ui.activity.main.MainPresenter
import dimysiklapo4ka.com.bezplatka.ui.fragment.advertisement.AddAdvertisementFragment
import dimysiklapo4ka.com.bezplatka.ui.fragment.main.adapter.AdvertisementAdapter

class MainFragment: BaseFragment<MainContract.MainView, MainContract.MainPresenter>(), ValueEventListener{

    override val resLayout: Int = R.layout.fragment_main
    override var presenter: MainContract.MainPresenter? =
        MainPresenter()

    private val adapter = AdvertisementAdapter()
    private var databaseReference: DatabaseReference? = null

    override fun init() {
        val database = FirebaseDatabase.getInstance()
        advertisementList.layoutManager = LinearLayoutManager(activity)
        advertisementList.adapter = adapter
        databaseReference = database.reference.child("advertisement")
        databaseReference?.addValueEventListener(this)
        fab.setOnClickListener { (activity as MainActivity).replacesFragment(R.id.container, AddAdvertisementFragment()) }
    }

    override fun onCancelled(p0: DatabaseError) {
    }

    override fun onDataChange(p0: DataSnapshot) {
        if (p0.exists()) {
            val dataSnapshot = mutableListOf<DataSnapshot>()
                p0.children.forEach {
                    dataSnapshot.add(it)
            }
            adapter.updateData(dataSnapshot)
        }
    }
}