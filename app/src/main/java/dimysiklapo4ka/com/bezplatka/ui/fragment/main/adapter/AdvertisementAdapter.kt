package dimysiklapo4ka.com.bezplatka.ui.fragment.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.R
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel
import dimysiklapo4ka.com.bezplatka.ui.fragment.main.viewholder.AdvertisementViewHolder

class AdvertisementAdapter: RecyclerView.Adapter<AdvertisementViewHolder>(){

    private var advertisements = mutableListOf<DataSnapshot>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdvertisementViewHolder = AdvertisementViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.view_item_advertisement, p0, false))

    override fun getItemCount(): Int = advertisements.size

    override fun onBindViewHolder(p0: AdvertisementViewHolder, p1: Int) {
        advertisements[p1]?.let { p0.bindView(it) }
    }

    fun updateData(adv: MutableList<DataSnapshot>){
        adv.let {
            if (it.isNotEmpty()) {
                advertisements.clear()
                advertisements.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

}