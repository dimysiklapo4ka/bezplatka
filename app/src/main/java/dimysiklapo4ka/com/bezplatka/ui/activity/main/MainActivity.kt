package dimysiklapo4ka.com.bezplatka.ui.activity.main

import android.support.v7.app.AlertDialog
import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.R
import dimysiklapo4ka.com.bezplatka.base.BaseActivity
import dimysiklapo4ka.com.bezplatka.replaceFragment
import dimysiklapo4ka.com.bezplatka.ui.fragment.advertisement.AddAdvertisementFragment
import dimysiklapo4ka.com.bezplatka.ui.fragment.main.MainFragment
import dimysiklapo4ka.com.bezplatka.ui.listeners.OnAdvertisementItemUpdateListeners

class MainActivity : BaseActivity<MainContract.MainView, MainContract.MainPresenter>(),
    MainContract.MainView {

    override var presenter: MainContract.MainPresenter? = MainPresenter()
    override val resLayout: Int = R.layout.activity_main

    private val addAdvertisementFragment = AddAdvertisementFragment()
    private var updateAdvertisement: OnAdvertisementItemUpdateListeners? = null

    override fun advertisementClicked(advertisement: DataSnapshot) {
        createAlertFromItemClick(advertisement).show()
    }

    override fun init() {
        replaceFragment(R.id.container, MainFragment())
        updateAdvertisement = addAdvertisementFragment
    }

    private fun createAlertFromItemClick(advertisement: DataSnapshot)
            : AlertDialog = AlertDialog.Builder(this).
        setTitle("Выберите действие").
        setMessage("Подтвердите ваш шаг").
        setPositiveButton("Изменить") { _, _ ->
            replacesFragment(R.id.container, addAdvertisementFragment)
            updateAdvertisement?.onAdvertisementItemUpdate(advertisement)
        }.
        setNegativeButton("Удалить") { _, _ -> presenter?.deleteItemInFirebaseDatabase(advertisement)}.
        setNeutralButton("Отмена") { dialog, _ -> dialog.dismiss()}.
        create()

}
