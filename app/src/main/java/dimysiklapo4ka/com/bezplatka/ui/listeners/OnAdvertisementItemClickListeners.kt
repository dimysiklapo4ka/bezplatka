package dimysiklapo4ka.com.bezplatka.ui.listeners

import com.google.firebase.database.DataSnapshot

interface OnAdvertisementItemUpdateListeners{
    fun onAdvertisementItemUpdate(advertisement: DataSnapshot)
}