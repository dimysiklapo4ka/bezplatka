package dimysiklapo4ka.com.bezplatka.ui.activity.main

import com.google.firebase.database.*
import dimysiklapo4ka.com.bezplatka.base.BasePresenterImplementation
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel

class MainPresenter : BasePresenterImplementation<MainContract.MainView>(),
    MainContract.MainPresenter {

    private val database = FirebaseDatabase.getInstance()
    private var databaseReference: DatabaseReference? = null

    init {
        databaseReference = database.reference
    }

    override fun addedOrUpdateItemInFirebaseDatabase(advertisementItem: DataSnapshot?,
                                              title: String, description: String, price: String, country: String,
                                              name: String, phone : String) {

        if (advertisementItem==null){
            databaseReference?.child("advertisement")!!.push().setValue(
                AdvertisementModel(title, description, price,country,name,phone)
            )
        }else {

            advertisementItem.ref.setValue(
                AdvertisementModel(title, description, price, country, name, phone)
            )
        }
    }

    override fun deleteItemInFirebaseDatabase(advertisementItem: DataSnapshot) {
        advertisementItem.ref.removeValue()
    }

}