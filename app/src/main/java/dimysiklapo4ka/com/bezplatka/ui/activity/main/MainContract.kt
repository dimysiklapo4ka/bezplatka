package dimysiklapo4ka.com.bezplatka.ui.activity.main

import com.google.firebase.database.DataSnapshot
import dimysiklapo4ka.com.bezplatka.base.BasePresenter
import dimysiklapo4ka.com.bezplatka.base.BaseView
import dimysiklapo4ka.com.bezplatka.model.AdvertisementModel

interface MainContract{
    interface MainView: BaseView{

    }

    interface MainPresenter: BasePresenter<MainView>{
        fun addedOrUpdateItemInFirebaseDatabase(advertisementItem: DataSnapshot?,
                                         title: String, description: String, price: String, country: String,
                                         name: String, phone : String)
        fun deleteItemInFirebaseDatabase(advertisementItem: DataSnapshot)
    }
}