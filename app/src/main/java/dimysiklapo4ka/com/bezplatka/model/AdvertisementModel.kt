package dimysiklapo4ka.com.bezplatka.model

data class AdvertisementModel(
//    val id: String? = null,
    val title: String? = null,
    val description: String? = null,
    val price: String? = null,
    val city: String? = null,
    val name_saler: String? = null,
    val phone_number: String? = null
)

