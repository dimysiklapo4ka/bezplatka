package dimysiklapo4ka.com.bezplatka

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

fun AppCompatActivity.replaceFragment(container: Int, fragment: Fragment, needToAddToBackStack: Boolean = false){
    val name = fragment.javaClass.simpleName
    supportFragmentManager.beginTransaction().apply {
        replace(container, fragment, name)
        if (needToAddToBackStack) addToBackStack(name)
    }.commit()
}