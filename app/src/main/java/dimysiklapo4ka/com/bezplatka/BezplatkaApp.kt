package dimysiklapo4ka.com.bezplatka

import android.app.Application
import com.google.firebase.FirebaseApp

class BezplatkaApp: Application(){

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
    }
}